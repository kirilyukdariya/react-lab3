import './App.css';
import {useEffect, useState} from "react";
import MyTable from "./Components/MyTable";

function App() {
    const [images, setImages] = useState([]);

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then((res) => res.json())
            .then((data) => {
                    let filteredRecords = data.filter(title => title.title.split(' ').length < 8);
                    setImages(filteredRecords);
                }
            )
    }, []);
    return (
        <div className="App">
            <MyTable images={images}/>
        </div>
    );
}

export default App;
