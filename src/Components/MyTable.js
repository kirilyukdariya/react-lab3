import * as React from 'react';
import {useState} from "react";
import {Link} from "@mui/material";

import MyModal from "./MyModal";
import MyVirtualizedTable from "./MyVirtualizedTable";

const MyTable = ({images}) => {
    const [modalImage, setModalImage] = useState(null);
    const [modalOpen, setModalOpen] = useState(null);

    const myModalShow = (e) => {
        let image= images.find(img => img.thumbnailUrl === e.target.textContent).url;
        setModalImage(image);
        setModalOpen(true);
    }

    return (
        <div style={{height: "700px"}}>
            <MyVirtualizedTable
                rowCount={images.length}
                rowGetter={({index}) => images[index]}
                columns={[
                    {
                        width: 50,
                        label: "Id",
                        dataKey: "id",

                    },
                    {
                        width: 90,
                        label: "Album Id",
                        dataKey: "albumId",
                    },
                    {
                        width: 500,
                        label: "Title",
                        dataKey: "title",
                    },
                    {
                        width: 250,
                        label: "Image URL",
                        dataKey: "thumbnailUrl",
                        cellRenderer: ({cellData}) => <Link component="button" onClick={myModalShow}>
                            {cellData}
                        </Link>
                    },
                ]}
            />
            <MyModal open={modalOpen} setOpen={setModalOpen} image={modalImage}/>
        </div>
    );
}

export default MyTable;