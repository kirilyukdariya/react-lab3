import React from "react";
import {Box, Modal} from "@mui/material";

export const MyModal = (props) => {
    return (
        <div>
            <Modal
                open={props.open}
                onClose= {() => props.setOpen(false)}>
                <Box sx={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    bgcolor: "white",
                    p: 2}}>
                    <img
                        alt='-'
                        src={props.image}/>
                </Box>
            </Modal>
        </div>
    );
};
 export default MyModal;